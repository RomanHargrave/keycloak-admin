<?php

namespace Scito\Keycloak\Admin;

class AuthBuilder {
    private $grantType;
    private $clientId;
    private $clientSecret;
    private $username;
    private $password;

    public function __construct()
    {
    }

    /**
     * Sets grant type to client_credentials
     * @return AuthBuilder
     */
    public function withClientCredentialsGrant(): self
    {
        $this->grantType = "client_credentials";
        return $this;
    }

    /**
     * Sets grant type to password
     * @return AuthBuilder
     */
    public function withPasswordGrant(): self
    {
        $this->grantType = "password";
        return $this;
    }

    /**
     * Sets username for password grant
     * @param string $username
     * @return AuthBuilder
     */
    public function withUsername(?string $username): self
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Sets password for password grant type
     * @param string $password
     * @return AuthBuilder
     */
    public function withPassword(?string $password): self
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Sets client ID for grant request
     * @param string $clientId
     * @return AuthBuilder
     */
    public function withClientId(string $clientId): self
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Sets client secret for client_credentials grant type
     * @param string $clientSecret
     * @return AuthBuilder
     */
    public function withClientSecret(?string $clientSecret): self
    {
        $this->clientSecret = $clientSecret;
        return $this;
    }

    public function build()
    {
        $auth_body = [
            'grant_type' => $this->grantType,
            'client_id'  => $this->clientId
        ];

        if ($this->grantType == 'client_credentials') {
            $auth_body['client_secret'] = $this->clientSecret;
        } elseif ($this->grantType == 'password') {
            $auth_body['username'] = $this->username;
            $auth_body['password'] = $this->password;
        }

        return $auth_body;
    }
}
