<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotImpersonateUserException extends RuntimeException
{
}
