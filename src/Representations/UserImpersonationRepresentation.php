<?php

namespace Scito\Keycloak\Admin\Representations;
use function get_defined_vars;

class UserImpersonationRepresentation
    extends AbstractRepresentation
    implements UserImpersonationRepresentationInterface
{
    public function __construct(
        string $redirect = null,
        bool $sameRealm = null,
        $identityCookie = null,
        $sessionCookie = null
    )
    {
        $this->setAttributes(get_defined_vars());
    }

    public function getRedirect(): ?string
    {
        return $this->getAttribute('redirect');
    }

    public function getSameRealm(): ?bool
    {
        return $this->getAttribute('sameRealm');
    }

    public function getIdentityCookie() {
        return $this->getAttribute('identityCookie');
    }

    public function setIdentityCookie($identityCookie) {
        $this->setAttribute('identityCookie', $identityCookie);
    }

    public function getSessionCookie() {
        $this->getAttribute('sessionCookie');
    }

    public function setSessionCookie($sessionCookie) {
        $this->setAttribute('sessionCookie', $sessionCookie);
    }
}
