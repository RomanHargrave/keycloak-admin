<?php

namespace Scito\Keycloak\Admin\Representation;

interface UserImpersonationRepresentationInterface extends RepresenationInterface {
    public function getRedirect(): string;
    public function getSameReaim(): bool;
    public function getIdentityCookie();
    public function getSessionCookie();
}
