<?php

namespace Scito\Keycloak\Admin\Token;

use DateInterval;
use GuzzleHttp\Client;
use RuntimeException;
use function date_create;
use function json_decode;

class TokenManager
{
    /**
     * @var Token[]
     */
    private $tokens;

    private $client;

    private $authRequest;

    public function __construct(array $authRequest, Client $client)
    {
        $this->client = $client;
        $this->authRequest = $authRequest;
    }

    /**
     * @param $realm
     * @return Token
     */
    public function getToken($realm)
    {
        if (isset($this->tokens[$realm]) && $this->tokens[$realm]->isValid()) {
            return $this->tokens[$realm];
        }

        $response = $this->client->post("/realms/{$realm}/protocol/openid-connect/token", ['form_params' => $this->authRequest]);

        if (200 !== $response->getStatusCode()) {
            throw new RuntimeException("Error getting token");
        }

        $data = json_decode((string)$response->getBody(), true);

        $expires = date_create()->add(new DateInterval(sprintf('PT%dS', $data['expires_in'])));

        $this->tokens[$realm] = new Token($data['token_type'], $data['access_token'], $expires);

        return $this->tokens[$realm];
    }
}
