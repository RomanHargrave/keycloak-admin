<?php

namespace Scito\Keycloak\Admin;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;
use Scito\Keycloak\Admin\Guzzle\DefaultHeadersMiddleware;
use Scito\Keycloak\Admin\Guzzle\TokenMiddleware;
use Scito\Keycloak\Admin\Hydrator\Hydrator;
use Scito\Keycloak\Admin\Resources\ResourceFactory;
use Scito\Keycloak\Admin\Token\TokenManager;

class ClientBuilder
{
    private $guzzle;

    private $realm;

    private $authRealm;

    private $serverUrl;

    private $clientId;

    private $authRequest;

    private $tokenManager;

    private $guzzleCfg;

    public function __construct()
    {
        $this->realm = 'master';
        $this->guzzleCfg = [
            'http_errors' => false
        ];
    }

    /**
     * @param ClientInterface $guzzle
     * @return ClientBuilder
     */
    public function withGuzzle(ClientInterface $guzzle): self
    {
        $this->guzzle = $guzzle;
        return $this;
    }

    /**
     * @param array $options
     * @return ClientBuilder
     */
    public function withGuzzleOptions(array $options): self
    {
        $this->guzzleCfg = $options + $this->guzzleCfg;
        return $this;
    }

    /**
     * @param string $realm
     * @return ClientBuilder
     */
    public function withRealm(string $realm): self
    {
        $this->realm = $realm;
        return $this;
    }

    /**
     * @param string $authRealm realm to authenticate with
     * @return ClientBuilder
     */
    public function withAuthRealm(string $authRealm): self
    {
        $this->authRealm = $authRealm;
        return $this;
    }

    /**
     * @param string $url
     * @return ClientBuilder
     */
    public function withServerUrl(string $url): self
    {
        $this->serverUrl = $url;
        return $this;
    }

    /**
     * @param null|string $clientId
     * @return ClientBuilder
     */
    public function withClientId(?string $clientId): self
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * The authRequest field is simply an associative array of form
     * parameters to be passed to the OIDC token endpoint.
     * See AuthBuilder, which assembles these parameters, for details.
     *
     * Overwrites any existing request.
     * @param null|string $req
     * @return ClientBuilder
     */
    public function withAuthRequest(array $req): self
    {
        $this->authRequest = $req;
        return $this;
    }

    public function build()
    {
        $stack = HandlerStack::create();

        $guzzle = $this->guzzle ?? new GuzzleClient(['base_uri' => $this->serverUrl] + $this->guzzleCfg);

        // hand off the auth request form data to the token manager,
        // unless one already exists
        if (null == ($tokenManager = $this->tokenManager)) {
            $tokenManager = $this->buildTokenManager($guzzle);
        }

        // and then create our guzzle middleware using that token
        // manager
        $tokenMiddleware = new TokenMiddleware($tokenManager, $this->realm, $this->authRealm ?? $this->realm);

        $stack->push($tokenMiddleware);
        $stack->push(new DefaultHeadersMiddleware());

        $client = new GuzzleClient(['handler' => $stack, 'base_uri' => $this->serverUrl] + $this->guzzleCfg);

        $factory = new ResourceFactory($client, new Hydrator());

        return new Client($factory, $this->realm, $this->clientId);
    }

    /**
     * @return ClientInterface
     */
    private function buildGuzzle()
    {
        return new GuzzleClient(['base_uri' => $this->serverUrl]);
    }


    private function buildTokenManager(ClientInterface $guzzle)
    {
        return new TokenManager($this->authRequest, $guzzle);
    }
}
