<?php

namespace Scito\Keycloak\Admin\Tests;

use Scito\Keycloak\Admin\Client;
use Scito\Keycloak\Admin\ClientBuilder;
use Scito\Keycloak\Admin\AuthBuilder;
use Scito\Keycloak\Admin\Representations\ClientRepresentationInterface;
use Scito\Keycloak\Admin\Representations\RoleRepresentationInterface;
use Scito\Keycloak\Admin\Tests\Traits\WithTestClient;

class ClientsTest extends TestCase
{
    use WithTestClient;

    /**
     * @test
     */
    public function clients_can_be_build()
    {
        $server = "http://localhost:8080";
        $clientId = "mykeycloak_client";
        $keycloakUsername = "admin";
        $keycloakPassword = "some passphrase";

        $auth = (new AuthBuilder())
              ->withPasswordGrant()
              ->withClientId($clientId)
              ->withUsername($keycloakUsername)
              ->withPassword($keycloakPassword)
              ->build();

        $client = (new ClientBuilder())
                ->withServerUrl($server)
                ->withClientId($clientId)
                ->withAuthRequest($auth)
                ->build();

        $this->assertInstanceOf(Client::class, $client);
    }

    /**
     * @test
     */
    public function clients_can_be_retrieved()
    {
        $client = $this->client
            ->realm('master')
            ->clients()
            ->all()
            ->first(function (ClientRepresentationInterface $client) {
                return 'account' === $client->getClientId();
            });

        $this->assertInstanceOf(ClientRepresentationInterface::class, $client);
    }

    /**
     * @test
     */
    public function client_roles_can_be_retrieved()
    {

        // Find the id of the client "account"
        /* @var $client ClientRepresentationInterface */
        $client = $this->client
            ->realm('master')
            ->clients()
            ->all()
            ->filter(function (ClientRepresentationInterface $client) {
                return $client->getClientId() == 'account';
            })
            ->first();

        $role = $this->client
            ->realm('master')
            ->client($client->getId())
            ->roles()
            ->all()
            ->first(function (RoleRepresentationInterface $role) {
                return 'manage-account' === $role->getName();
            });

        $this->assertInstanceOf(RoleRepresentationInterface::class, $role);

    }
}
